/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

AJS.$(document).ready(function() {

    function notABot(user) {
        const re = /.*-bot.*/;
        return !re.exec(user.id);
    }

    AJS.$(".owner-autocomplete").autocomplete({
        minLength: 2,
        source: function (request, response) {
            AJS.$.ajax({
                type: "GET",
                url: AJS.contextPath() + "/rest/api/latest/search/users?searchTerm=" + request.term,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    const validUsers = data.searchResults.filter(notABot);
                    response(AJS.$.map(validUsers, function( user ) {
                            return {
                                label: user.id + " (" + user.searchEntity.fullName + ")",
                                value: user.id
                            }
                    }));

                }
            });
        }
    });
});
