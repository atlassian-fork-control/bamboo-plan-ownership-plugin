<html>
<head>
    <meta name="decorator" content="atl.admin">
    <title>[@ww.text name='planownership.bamboo.plugin.admin.title' /]</title>
</head>
<body>
[@ui.bambooSection titleKey='planownership.bamboo.plugin.admin.title']
    [@ww.form action="configPlanOwnershipEnabler.action" method="post" class="aui"]
        [@ww.checkbox id='enableEnforcement_id' labelKey='planownership.bamboo.plugin.admin.enableEnforcement' name='enableEnforcement' /]
        [@ww.checkbox id='enablePlanDisablement_id' labelKey='planownership.bamboo.plugin.admin.enablePlanDisablement' name='enablePlanDisablement' /]
        [@ww.submit value="Save" theme="simple" cssClass="aui-button"/]
    [/@ww.form]
[/@ui.bambooSection]

</body>
</html>
